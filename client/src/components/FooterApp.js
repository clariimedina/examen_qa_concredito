import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = {
  footerContainer:{
    position: 'fixed',
    left: 0,
    bottom: 0,
    width: '100%',
    backgroundColor: '#424242',
    color: '#fff',
    textAlign: 'center' 
  }
};

class FooterApp extends React.Component {
  
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.footerContainer}>
        <Typography variant="overline" display="block">
            Made with ❤️ by Clarissa Medina
        </Typography>
      </div>
    );
  }
}

export default withStyles(useStyles)(FooterApp);

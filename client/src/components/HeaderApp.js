import React from 'react';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import  Anagram  from './Apps/Anagram';
import  Students  from './Apps/Students';
import  Kareoke  from './Apps/Kareoke';
import { withStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';

const useStyles = {
  root: {
    height: 100,
    textAlign: 'center',
    padding: '20px 0'
  },
  appContainer:{    
    margin: '20px 50px',
    borderRadius: 5
  }
};

class HeaderApp extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      appSelectedId: 0,
      showSuccessAlert: false,
      showErrorAlert: false
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleShowSuccessAlert = this.handleShowSuccessAlert.bind(this);
    this.handleShowErrorAlert = this.handleShowErrorAlert.bind(this);
  }

  handleChange(event, app){
    this.setState({ appSelectedId: app})
  }

  handleShowSuccessAlert(value){
    this.setState({showSuccessAlert: value})
  }

  handleShowErrorAlert(value){
    this.setState({showErrorAlert: value})
  }
  
  render() {
    const { classes } = this.props
    const { showSuccessAlert, showErrorAlert } = this.state;
    const apps = [
      {
        id: 0,
        name: 'Anagrama',
        component: <Anagram/>
      },
      {
        id: 1,
        name: 'Rockola',
        component: <Kareoke
          handleShowSuccessAlert={this.handleShowSuccessAlert }
          handleShowErrorAlert={this.handleShowErrorAlert}
        />
      },
      {
        id: 2,
        name: 'Alumnos',
        component: <Students
          handleShowSuccessAlert={this.handleShowSuccessAlert }
          handleShowErrorAlert={this.handleShowErrorAlert}
        />
      }
    ]
    return (
      <section>
        <section className={classes.root}>
          <Typography variant="overline" display="block" gutterBottom>
            Concredito
          </Typography>
          <Typography variant="h5" gutterBottom>
              Evaluación para responsable de pruebas automatizadas
          </Typography>
          <Typography variant="overline" display="block" gutterBottom>
          ---------- Viernes 3 de Septiembre 2021 ----------
          </Typography>
        </section>
        <section>
        <AppBar position="static">
          <Tabs value={this.state.appSelectedId} onChange={this.handleChange}>
            {apps.map((app) => (
              <Tab key={app.id} label={app.name} />
            ))}
          </Tabs>
        </AppBar>
        {apps.map((app) => (
          <div key={app.id} className={classes.appContainer}>
            { this.state.appSelectedId === app.id && app.component}
          </div>
        ))}
        </section>
        { showSuccessAlert && (
          <Alert severity="success">El registro se ha guardado con exito</Alert>
        )}
        { showErrorAlert && (
          <Alert severity="error">Faltan campos por completar</Alert>
        )}
      </section>
    );
  }
}

export default  withStyles(useStyles)(HeaderApp);

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MomentUtils from '@date-io/moment';
import { KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Axios from 'axios'

const useStyles = {
  formContainer:{
    marginTop: 50,
    color: '#455a64',
    height: 650
  },
  formItem:{
    marginTop: 50
  },
  textFieldItem: {
    margin: 15,
    width: '20%'
  },
  gradesCaptureContainer:{
    marginTop: 70
  },
  titleText:{
    color: '#1c313a'
  },
  headerFormContainer: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  buttonSaveContainer:{
    textAlign: 'right'
  }
};

class StudentForm extends React.Component {
  constructor(props){
    super(props)
    
    const { student } = props;

    this.state = {
      nameStudent: student ? student.name : false,
      lastNameStudent: student ? student.lastName : false,
      SecondlastNameStudent: student ? student.secondLastName : false,
      birthDateStudent: student ? student.birthDate : false,
      grades: student ? student.ReportCards : []
    }
    
    this.changeStateTextField = this.changeStateTextField.bind(this);
    this.saveNewStudent = this.saveNewStudent.bind(this);
    this.changeStateBirthDate = this.changeStateBirthDate.bind(this);
    this.changeStateGrades = this.changeStateGrades.bind(this);
    this.validateTextFields = this.validateTextFields.bind(this);
  }

  changeStateTextField(e) {
    const nameInput = e.target.id;
    const valueInput = e.target.value;

    this.setState({ [nameInput]: valueInput});
  }

  saveNewStudent(studentEdit = false) {
    const { grades, nameStudent, lastNameStudent, SecondlastNameStudent, birthDateStudent } = this.state
    const { getStudents,addNewStudent, handleShowAlert, handleShowErrorAlert } = this.props;

    if(this.validateTextFields()){
      const student = {
        name: nameStudent,
        lastName: lastNameStudent,
        secondLastName: SecondlastNameStudent,
        birthDate: birthDateStudent,
        grades: grades,
        id: studentEdit ? studentEdit.id : ''
      }
      
      if(!studentEdit){
        Axios.post("http://localhost:3002/student", student).then(()=> {
          getStudents();
          addNewStudent(false)
          handleShowAlert(true);
          setTimeout(()=> {
            handleShowAlert(false);
          }, 3000);
        })
      } else {
        Axios.post("http://localhost:3002/student/edit", student).then(()=> {
          getStudents();
          addNewStudent(false)
          handleShowAlert(true);
          setTimeout(()=> {
            handleShowAlert(false);
          }, 3000);
        })
      }
    } else {
      handleShowErrorAlert(true);
      setTimeout(()=> {
        handleShowErrorAlert(false);
      }, 3000);
    }
  }

  validateTextFields(){
    const { grades, nameStudent, lastNameStudent, SecondlastNameStudent, birthDateStudent } = this.state
    const { courses } = this.props;

    const generalDataFields =  ((nameStudent || nameStudent !== '') && 
    (lastNameStudent ||lastNameStudent !== '') && 
    (SecondlastNameStudent ||SecondlastNameStudent !== '') && 
    (birthDateStudent ||birthDateStudent !== ''));

    const gradeFields = grades.every((grade) => grade.grade !== '');

    return (generalDataFields && gradeFields && (grades.length === courses.length));
  }

  changeStateBirthDate(date) {
    this.setState({birthDateStudent: date.format('YYYY-MM-DD')});
  }

  changeStateGrades(idCoursedField, value){
    const { grades } = this.state;
    const { student } = this.props;
    const newGrades = grades;

    if(grades.length > 0){  
      const gradeFoundIndex = newGrades.findIndex(grade => grade.courseId === idCoursedField);
      
      if(gradeFoundIndex >= 0) {
        newGrades[gradeFoundIndex].grade = parseInt(value);
        if(student){
          const saveGrade = grades.find((grade) => grade.courseId === idCoursedField)
          newGrades[gradeFoundIndex].id = saveGrade.id;
        }
      } else {
        newGrades.push({
          courseId: idCoursedField,
          grade: value
        })  
      }
    } else {
      newGrades.push({
        courseId: idCoursedField,
        grade: value
      })
    }
    this.setState({grades: newGrades});
  }
  
  render() {
    const { classes, courses, addNewStudent, student } = this.props;
    const { birthDateStudent, grades } = this.state;
    
    return (
      <section className={classes.formContainer}>
        <div className={classes.headerFormContainer}>
          <Typography variant="h4" display="block" className={classes.titleText}>
            Captura nuevo alumno      
          </Typography>
          <IconButton aria-label="close" onClick={() => {addNewStudent(false)}}>
            <CloseIcon />
          </IconButton>
        </div>
        <form className={classes.formItem} noValidate autoComplete="off">
          <Typography variant="h6" display="block">
            Datos generales       
          </Typography>
          <div>
            <TextField
              required
              id="nameStudent"
              label="Nombre"
              variant="outlined"
              defaultValue={student.name ? student.name : ''}
              onBlur={this.changeStateTextField}
              className={classes.textFieldItem}
            />
            <TextField
              id="lastNameStudent"
              label="Apellido Paterno"
              variant="outlined"
              defaultValue={student.lastName ? student.lastName : ''}
              onBlur={this.changeStateTextField}
              className={classes.textFieldItem}
            />
            <TextField
              id="SecondlastNameStudent"
              label="Apellido Materno"
              variant="outlined"
              defaultValue={student.secondLastName ? student.secondLastName : ''}
              onBlur={this.changeStateTextField}
              className={classes.textFieldItem}
            />
            <MuiPickersUtilsProvider utils={MomentUtils}>
              <KeyboardDatePicker
                margin="normal"
                id="birthDateStudent"
                label="Fecha de nacomiento"
                format="MM/DD/yyyy"
                value={birthDateStudent}
                onChange= {this.changeStateBirthDate}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
          </div>
          <section className={classes.gradesCaptureContainer}>
            <Typography variant="h6" display="block">
              Captura de Calificaciones       
            </Typography>
            <div>
              {courses.map((course) => {
                const idField = "course_" + course.id;
                let grade = '';
                if(student){
                  let gradeFind = grades.find((grade)=> grade.courseId === course.id);
                  grade = gradeFind.grade;
                }
                
                return (
                  <TextField
                    key={course.id}
                    required
                    id={idField} 
                    label={course.Name}
                    variant="outlined"
                    type="course"
                    defaultValue={grade}
                    onBlur={(e) => {this.changeStateGrades(course.id, e.target.value)}}
                    className={classes.textFieldItem}
                  />
                )})}
            </div>
          </section>
          <div className={classes.buttonSaveContainer}>
            <Button color="primary" onClick={(e)=>{this.saveNewStudent(student)}} variant="contained">Guardar</Button>
          </div>
        </form>
      </section>
    );
  }
}

export default withStyles(useStyles)(StudentForm);

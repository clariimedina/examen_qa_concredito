import React from 'react';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Alert from '@material-ui/lab/Alert';

const useStyles = {
  root: {
    padding: '30px 10px'
  },
  fieldAnagram:{
    width: '30%',
    margin: 20
  }
};

class Anagram extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isAnagram: false,
      string1: '',
      string2: ''
    }
    this.areAnagrams = this.areAnagrams.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  areAnagrams(string1, string2) {
    
    let checkAnagram = true;
    let letters = {
      string1: {},
      string2: {}
    },
    currentChar;
      
    string1 = string1.toLowerCase();
    string2 = string2.toLowerCase();
    
    string1 = string1.replace(/[^a-z]/g, '');
    string2 = string2.replace(/[^a-z]/g, '');
      
    for(let i = 0; i < string1.length; i++){
      currentChar = string1.charAt(i);
        if(letters.string1[currentChar]){
          letters.string1[currentChar]++;
        } else {
          letters.string1[currentChar] = 1;
        }
    }
      
    for(let i = 0; i < string2.length; i++){
      currentChar = string2.charAt(i);
        if(letters.string2[currentChar]){
          letters.string2[currentChar]++;
        } else {
          letters.string2[currentChar] = 1;
        }
    }
      
    var keys ={
        string1: Object.keys(letters.string1),
        string2: Object.keys(letters.string2)
    };
    
    if(keys.string1.length !== keys.string2.length){
      checkAnagram = false;
    }
    else {
      for(let element in keys.string1){
        let key = keys.string1[element];
        if(letters.string1[key] && letters.string2[key] && letters.string1[key] === letters.string2[key]){
          continue;
        }
        checkAnagram = false;
      }
    }
    
    this.setState({ isAnagram: checkAnagram })


  }

  handleChange(event) {
    event.persist();
    const fieldName = event.target.name;
    const string1 = event.target.name === 'string1' ? event.target.value : this.state.string1;
    const string2 = event.target.name === 'string2' ? event.target.value : this.state.string2;
    this.setState({[fieldName]: event.target.value});
    this.areAnagrams(string1, string2);
  }
  
  render(){
    const { classes } = this.props;
    const { isAnagram, string1, string2 } = this.state;

    return(
      <div className={classes.root}>
        <Typography variant="h4" display="block" className={classes.titleText}>
          Verificar anagramas    
        </Typography>
        <form noValidate autoComplete="off">
          <TextField
            label="Palabra 1"
            variant="outlined"
            name="string1"
            onChange={this.handleChange}
            className={classes.fieldAnagram}/>
          <TextField 
            label="Palabra 2"
            variant="outlined"
            name="string2"
            onChange={this.handleChange}
            className={classes.fieldAnagram}
          />
        </form>
        { string1 !== '' && string2 !== '' && (
          <section>
            {isAnagram
              ? 
                (
                  <Alert severity="success">¡Si son anagramas!</Alert>
                )
              : (
                  <Alert severity="error">No son anagramas </Alert>
                )
            }
          </section>
        )}
      </div>
    )
  }
}

export default withStyles(useStyles)(Anagram);
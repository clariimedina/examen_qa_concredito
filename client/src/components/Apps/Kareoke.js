import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';  
import { withStyles } from '@material-ui/core/styles';
import Axios from 'axios'
import TextField from '@material-ui/core/TextField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import KareokePlayer from './KareokePlayer';
import CloseIcon from '@material-ui/icons/Close';
import SongsFiltered from './SongsFiltered';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import MicIcon from '@material-ui/icons/Mic';
import Button from '@material-ui/core/Button';
import NewSongForm from './NewSongForm'
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = {
  KareokeList: {
    listStyle: 'none',
    margin: 0,
    padding: 0
  },
  titlesText:{
    backgroundColor: '#66bb6a',
    color: 'white',
    padding: 8,
    borderRadius: 10
  },
  kareokeGender: {
    padding: '15px 0 20px 20px'
  },
  kareokeGenderItem:{
    cursor: 'pointer',
    margin: 5
  },
  kareokeGenderPaper: {
    padding: 50,
    minWidth: 150,
    textAlign: 'center',
    backgroundColor: "#eee"
  },
  searchContainer: {
    display:'block',
    margin: 10
  },
  searchForm:{
    padding: 15,
    width: '80%'
  },
  closeButton:{
    float: 'right',
  },
  genderIcon:{
    color:'#338a3e'
  },
  titlesTextArtist:{
    backgroundColor: '#ffb74d',
    color: 'white',
    padding: 8,
    borderRadius: 10
  },
  artistIcon:{
    color:'#c88719'
  },
  headerKareoke:{
    display: 'flex',
    justifyContent: 'space-between'
  },
  listSongsContainer:{
    height: 500
  }
};

class Kareoke extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      genders: [],
      artists: [],
      songs: [],
      onFocusSearch: false,
      songSelected: false,
      songsFilteredList: false,
      newSong: false,
      songDelete: false,
      songEdit: false,
      openConfirm: false
    }
    this.getGenders = this.getGenders.bind(this);
    this.getArtists = this.getArtists.bind(this);
    this.onFocusSearch = this.onFocusSearch.bind(this);
    this.onClickIconClose = this.onClickIconClose.bind(this);
    this.getSongs = this.getSongs.bind(this);
    this.handleClickPlay = this.handleClickPlay.bind(this);
    this.getSongsByGender = this.getSongsByGender.bind(this);
    this.getSongsByArtist = this.getSongsByArtist.bind(this);
    this.addNewSongs = this.addNewSongs.bind(this);
    this.handleOpenConfirmDialog = this.handleOpenConfirmDialog.bind(this);
    this.editSong = this.editSong.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.removeSong = this.removeSong.bind(this);
  
    this.getGenders()
    this.getArtists()
    this.getSongs()
  }
  getGenders() {
    Axios.get("http://localhost:3002/gender/genders", { crossdomain: true }).then((response) => {
      this.setState({genders: response.data })
    })
  }

  getArtists (){
    Axios.get("http://localhost:3002/artist/artists", { crossdomain: true }).then((response) => {
      this.setState({artists: response.data })
    })
  }
  getSongs() {
    Axios.get("http://localhost:3002/song/songs", { crossdomain: true }).then((response) => {
      this.setState({songs: response.data })
    })
  }
  onFocusSearch(e) {
    this.setState({onFocusSearch: true})
  }

  onClickIconClose(e){
    this.setState({onFocusSearch: false})
    this.setState({songSelected: false})
    this.setState({songsFilteredList: false})
    this.setState({newSong: false})
  }

  handleClickPlay(song){
    this.setState({songSelected: song})
  }

  getSongsByGender(genderId) {
    const { songs } = this.state;
    this.setState({onFocusSearch: true})
    const songsFilteredList = songs.filter(song => song.genderId === genderId )

    this.setState({songsFilteredList: songsFilteredList})
  }

  getSongsByArtist(artistId){
    const { songs } = this.state;
    this.setState({onFocusSearch: true})
    const songsFilteredList = songs.filter(song => song.artistId === artistId )

    this.setState({songsFilteredList: songsFilteredList})
  }

  addNewSongs(){
    this.setState({newSong: true})
  }

  handleOpenConfirmDialog(songId){
    this.setState({openConfirm: true})
    this.setState({songDelete: songId});
  }

  editSong(song){
    this.setState({songEdit: song})
    this.addNewSongs();
  }

  handleCancel(){
    this.setState({openConfirm: false});
  }

  handleConfirm(){
    const { songDelete } = this.state;

    this.removeSong(songDelete);
    this.setState({openConfirm: false});
    this.setState({songDelete: false});
  }

  removeSong(songId) {
    Axios.post("http://localhost:3002/song/delete", { id: songId }).then((response) => {
      this.getSongs();
    });
  }

  render(){
    
    const { classes, handleShowSuccessAlert, handleShowErrorAlert } = this.props;
    const { onFocusSearch, songSelected, songsFilteredList, newSong, genders, artists, songEdit, openConfirm } = this.state;

    return(
      <section>
          <section className={classes.searchContainer}>
          <section>
          { (onFocusSearch  || newSong) && (
              <IconButton aria-label="close" className={classes.closeButton} onClick={this.onClickIconClose}>
                <CloseIcon />
              </IconButton>
            )}
            { !songSelected &&  !newSong &&(
              <div className={classes.headerKareoke}>
                <form className={classes.searchForm} noValidate autoComplete="off">
                  <TextField 
                    id="outlined-basic" 
                    label="Buscar"
                    fullWidth
                    className={classes.searchTextField} 
                    onFocus={this.onFocusSearch}
                  />
                </form>
                <Button color="primary" onClick={this.addNewSongs}>Agregar cancion</Button>
              </div>
            )}
          </section>
          { onFocusSearch  && !songSelected && !songsFilteredList && !newSong && (
              <List component="nav">
                {this.state.songs.map((song) => (
                  <ListItem button key={song.id} onClick={()=>{this.handleClickPlay(song)}}>
                    <ListItemText
                      primary={song.name}
                      secondary= {song.Artist.name}
                    />
                    <ListItemSecondaryAction>
                    <IconButton aria-label="edit" onClick={(e)=>{this.editSong(song)}}>
                      <CreateIcon fontSize="small" />
                    </IconButton>
                    <IconButton aria-label="delete" onClick={(e)=>{this.handleOpenConfirmDialog(song.id)}}>
                      <DeleteIcon fontSize="small" />
                    </IconButton>
                  </ListItemSecondaryAction>
                  </ListItem>
                ))}
              </List>
          )}
        </section>
        { !onFocusSearch  && !songSelected && !songsFilteredList && !newSong &&(
          <ul className={classes.KareokeList}>
            <li>
              <Typography variant="h5" gutterBottom >
                <Box fontWeight="fontWeightLight" className={classes.titlesText}>
                  Generos
                </Box>
              </Typography>
              <Grid container className={classes.kareokeGender}>
                {this.state.genders.map((gender) => (
                  <div key={gender.id}>
                      <Grid item className={classes.kareokeGenderItem}>
                        <Paper className={classes.kareokeGenderPaper} onClick={() => {this.getSongsByGender(gender.id)}}>
                          <Typography variant="overline" display="block" gutterBottom>
                            {gender.name}
                          </Typography>
                          <MusicNoteIcon className={classes.genderIcon}/>
                        </Paper>
                      </Grid>
                  </div>
                ))}
              </Grid>
            </li>
            <li>
              <Typography variant="h5" gutterBottom>
                <Box fontWeight="fontWeightLight" className={classes.titlesTextArtist}>
                  Artistas
                </Box>
              </Typography>
              <Grid container className={classes.kareokeGender}>
                {this.state.artists.map((artist) => (
                  <div key={artist.id}>
                      <Grid item className={classes.kareokeGenderItem}>
                        <Paper className={classes.kareokeGenderPaper} onClick={() => {this.getSongsByArtist(artist.id)}}>

                          <Typography variant="overline" display="block" gutterBottom>
                            {artist.name}
                          </Typography>
                          <MicIcon  className={classes.artistIcon}/>
                        </Paper>
                      </Grid>
                  </div>
                ))}
              </Grid>
            </li>
          </ul>
        )}
        <section>
          { songSelected && (
            <KareokePlayer 
              song={songSelected}
            />
          )}
        </section>
        <section>
          { songsFilteredList && (
            <SongsFiltered 
              songs={songsFilteredList}
              handleClickPlay={this.handleClickPlay}
            />
          )}
        </section>
        <section>
          { newSong && (
            <NewSongForm
              genders={genders}
              artists={artists}
              song={songEdit}
              getGenders={this.getGenders}
              getArtists={this.getArtists}
              onClickIconClose={this.onClickIconClose}
              getSongs={this.getSongs}
              handleShowSuccessAlert={handleShowSuccessAlert}
              handleShowErrorAlert={handleShowErrorAlert}
            />
          )}
          <Dialog maxWidth="md" open={openConfirm}>
            <DialogTitle id="confirmation-dialog-title">Confirmacion</DialogTitle>
            <DialogContentText>
              ¿Deseas eliminar el registro?
            </DialogContentText>
            <DialogActions>
              <Button autoFocus onClick={this.handleCancel} color="primary">
                Cancel
              </Button>
              <Button onClick={this.handleConfirm} color="primary">
                Ok
              </Button>
            </DialogActions>
          </Dialog>
        </section>
      </section>
    )
  }
}

export default withStyles(useStyles)(Kareoke);
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = {
  videoPlayer: {
    width: '100%',
    height: '700px'
  },
  playerContainer: {
    listStyle: 'none' 
  },
  playerItem: {
    padding: 10
  }
};
class KareokePlayer extends React.Component {
  
  render(){
    const { song, classes } = this.props;
    const urlVideo = song.url + '?controls=0&autoplay=1';
    return(
      <section>
        <ul className={classes.playerContainer}>
          <li className={classes.playerItem}>
            <Typography variant="h3" gutterBottom>
              {song.name}
            </Typography>
            <Typography variant="h5" gutterBottom>
              {song.Artist.name}
            </Typography>
          </li>
          <li className={classes.playerItem}>
            <iframe 
            title="video_player"
            className={classes.videoPlayer}
            src= {urlVideo} 
            frameBorder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
            allowFullScreen />
          </li>
        </ul>
      </section>
    )
  }
}

export default  withStyles(useStyles)(KareokePlayer);
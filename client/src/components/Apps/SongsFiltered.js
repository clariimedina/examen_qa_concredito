import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import KareokePlayer from './KareokePlayer';

const useStyles = {
  videoPlayer: {
    width: '100%',
    height: '700px'
  }
};
class SongsFiltered extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      songSelected: false
    }
  }

  render(){
    const { songs, handleClickPlay } = this.props;
    const { songSelected } = this.state;
    console.log(songSelected,'song');
    
    return(
      <section>
      { !songSelected && (
        <List component="nav">
          {songs.map((song) => (
            <ListItem button key={song.id} onClick={()=>{handleClickPlay(song)}}>
              <ListItemText
                primary={song.name}
                secondary= {song.Artist.name}
              />
            </ListItem>  
          ))}
        </List>
      )}
      { songSelected && (
        <KareokePlayer 
          song={songSelected}
        />
      )}
      </section>
    )
  }
}

export default  withStyles(useStyles)(SongsFiltered);
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Axios from 'axios'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = {
  formContainer:{
    marginTop: 50,
    color: '#455a64'
  },
  titleText:{
    color: '#1c313a'
  },
  formItem:{
    marginTop: 50
  },
  textFieldItem: {
    margin: 15,
    width: '20%'
  },
  buttonSaveContainer:{
    textAlign: 'right'
  }
};

class NewSongForm extends React.Component {

  constructor(props){
    super(props)
    const { song } = props;
    this.state = {
      nameSong: song ? song.name : false,
      urlSong: song ? song.url :false,
      genderId: song ? song.genderId : false,
      artistId: song ? song.artistId : false,
      openDialog: false,
      artistDialog: false,
      genderDialog: false,
      Dialog: false,
      textFieldNombre: false
    }
    this.saveNewSong = this.saveNewSong.bind(this);
    this.changeStateTextField = this.changeStateTextField.bind(this);
    this.changeStateSelectField = this.changeStateSelectField.bind(this);
    this.saveNewGender = this.saveNewGender.bind(this);
    this.saveNewArtist = this.saveNewArtist.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this)
    this.handleChangeTextFieldNombre = this.handleChangeTextFieldNombre.bind(this);
    this.validateFields = this.validateFields.bind(this);
  }
  handleCloseDialog(){
    this.setState({openDialog: false});
    this.setState({artistDialog: false});
    this.setState({genderDialog: false});
  }
  handleOpenDialog(dialogKey){
    this.setState({openDialog: true});
    this.setState({[dialogKey]: true})
  }

  changeStateTextField(e) {
    const nameInput = e.target.id;
    const valueInput = e.target.value;

    this.setState({ [nameInput]: valueInput});
  }

  changeStateSelectField(e) {
    const nameInput = e.target.name;
    const valueInput = e.target.value;

    this.setState({ [nameInput]: valueInput});
  }

  saveNewSong(){  
    const { nameSong, urlSong, genderId, artistId } = this.state
    const { getSongs, onClickIconClose, handleShowErrorAlert, handleShowSuccessAlert, song } = this.props;

    if(this.validateFields()){
      const newSong = {
        name: nameSong,
        url: urlSong,
        genderId: genderId,
        artistId: artistId,
        id: song ? song.id : false
      }
      if(!song) {
        Axios.post("http://localhost:3002/song", newSong).then(()=> {
          getSongs();
          onClickIconClose();
          handleShowSuccessAlert(true);
          setTimeout(()=> {
            handleShowSuccessAlert(false);
          }, 3000);
        })
      } else {
        Axios.post("http://localhost:3002/song/edit", newSong).then(()=> {
          getSongs();
          onClickIconClose();
          handleShowSuccessAlert(true);
          setTimeout(()=> {
            handleShowSuccessAlert(false);
          }, 3000);
        })
      }
    } else {
      handleShowErrorAlert(true);
      setTimeout(()=> {
        handleShowErrorAlert(false);
      }, 3000);
    }
  }

  validateFields(){
    const { nameSong, urlSong, genderId, artistId } = this.state

    return((nameSong || nameSong!== '' ) && 
    (urlSong || urlSong !== '' ) && 
    (genderId || genderId !== 0 ) && 
    (artistId || artistId !== 0 ))
  }

  saveNewGender(){  
    const { textFieldNombre, handleShowSuccessAlert } = this.state
    const gender = {
      name: textFieldNombre,
    }
    if(textFieldNombre && textFieldNombre !== '') {
      Axios.post("http://localhost:3002/gender", gender).then(()=> {
        handleShowSuccessAlert(true);
        setTimeout(()=> {
          handleShowSuccessAlert(false);
        }, 3000);
        this.props.getGenders();
        this.handleCloseDialog()
      })
    } else {
      alert("El campo de nombre es requerido");
    }
  }

  saveNewArtist(){  
    const { textFieldNombre } = this.state
    const { handleShowSuccessAlert } = this.props;
    if(textFieldNombre && textFieldNombre !== '') {
      const artist = {
        name: textFieldNombre,
      }
      Axios.post("http://localhost:3002/artist", artist).then(()=> {
        handleShowSuccessAlert(true);
        setTimeout(()=> {
          handleShowSuccessAlert(false);
        }, 3000);
        this.props.getArtists();
        this.handleCloseDialog()
      })
    } else {
      alert("El campo de nombre es requerido");
    }
  }

  handleChangeTextFieldNombre(e){
    this.setState({textFieldNombre: e.target.value})
  }
  render() {
    const { openDialog, artistDialog } = this.state;
    const { classes, genders, artists, song } = this.props;
    return (
      <section className={classes.formContainer}>
        <Typography variant="h4" display="block" className={classes.titleText}>
            Captura nueva cancion      
        </Typography>
        <form className={classes.formItem} noValidate autoComplete="off">
          <div>
            <TextField
              required
              id="nameSong"
              label="Nombre"
              variant="outlined"
              defaultValue={ song ? song.name : ''}
              onBlur={this.changeStateTextField}
              className={classes.textFieldItem}
            />
            <TextField
              id="urlSong"
              label="URL Youtube"
              required
              variant="outlined"
              defaultValue={ song ? song.url : ''}
              onBlur={this.changeStateTextField}
              className={classes.textFieldItem}
            />
          </div>
          <div>
            <TextField
              id="genderList"
              select
              name="genderId"
              label="Generos"
              variant="filled"
              value={ song ? song.genderId : ''}
              className={classes.textFieldItem}
              onChange={this.changeStateSelectField}
            >
              {genders.map((gender) => (
                <MenuItem key={gender.id} value={gender.id}>
                  {gender.name}
                </MenuItem>
              ))}
            </TextField>
            <Button color="primary" onClick={(e) => {this.handleOpenDialog("genderDialog")}}>Agregar Genero</Button>
            <TextField
              id="genderList"
              name="artistId"
              select
              label="Artistas"
              variant="filled"
              value={ song ? song.artistId : ''}
              className={classes.textFieldItem}
              onChange={this.changeStateSelectField}
            >
              {artists.map((artist) => (
                <MenuItem key={artist.id} value={artist.id}>
                  {artist.name}
                </MenuItem>
              ))}
            </TextField>
            <Button color="primary" onClick={(e) => {this.handleOpenDialog("artistDialog")}}>Agregar Artista</Button>
          </div>
          <div className={classes.buttonSaveContainer}>
            <Button color="primary" variant="contained" onClick={this.saveNewSong}>Guardar</Button>
          </div>
        </form>
        <Dialog open={openDialog} onClose={this.handleCloseDialog} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">
            { artistDialog ? "Captura artista" : "Captura de genero"}
          </DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              id="name"
              label="Nombre"
              onChange={this.handleChangeTextFieldNombre}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCloseDialog} color="primary">
              Cancel
            </Button>
            <Button 
              onClick={artistDialog ? this.saveNewArtist : this.saveNewGender} 
              color="primary"
            >
              Guardar
            </Button>
          </DialogActions>
        </Dialog>
      </section>
    );
  }
}

export default withStyles(useStyles)(NewSongForm);

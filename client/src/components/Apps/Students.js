import React from 'react';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Axios from 'axios'
import { withStyles } from '@material-ui/core/styles';
import StudentForm from './StudentForm'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = {
  headerActionsContainer: {
    textAlign: 'right',
    padding: 15
  },
  tableStudentsContainer:{
    height: 500
  }
};
const columns = [
  { id: 'name', label: 'Nombre', minWidth: 170 },
  { id: 'lastName', label: 'Apellido paterno', minWidth: 100 },
  {
    id: 'secondLastName',
    label: 'Apellido materno',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'average',
    label: 'Promedio',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'birthDate',
    label: 'Fecha de nacimiento',
    minWidth: 170,
    align: 'right'
  },
  {
    id: 'actions',
    label: 'Acciones',
    minWidth: 170,
    align: 'center'
  },
];
class Students extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      page: 0,
      setPage: 0,
      setRowsPerPage: 10,
      rowsPerPage: 10,
      students: [],
      studentCapture: false,
      courses: false,
      studentSelected: false,
      openConfirm: false,
      studentDelete: false
    }
    this.getStudents = this.getStudents.bind(this);
    this.CalculateAverage = this.CalculateAverage.bind(this)
    this.addNewStudent = this.addNewStudent.bind(this);
    this.getCourses = this.getCourses.bind(this);
    this.editStudent = this.editStudent.bind(this);
    this.removeStudent = this.removeStudent.bind(this);
    this.getTopTenAverage = this.getTopTenAverage.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleOpenConfirmDialog = this.handleOpenConfirmDialog.bind(this);

    this.getStudents()
    this.getCourses();
  }

  getCourses(){
    Axios.get("http://localhost:3002/course/courses", { crossdomain: true }).then((response) => {
      this.setState({courses: response.data})
    }) 
  }
  CalculateAverage(students) {
    students.forEach(student => {
      const reportCards = student.ReportCards;
      let totalGrades = 0;
      let average = 0;
      
      if(reportCards.length > 0){
        reportCards.forEach(reportCard => {
          totalGrades += reportCard.grade;
        })
        average = (totalGrades / reportCards.length).toFixed(2);
      }
      student['average'] = average;
    })
    this.setState({students: students })
  }

  getStudents(){
    Axios.get("http://localhost:3002/student/students", { crossdomain: true }).then((response) => {  
      this.CalculateAverage(response.data);
    })
  }

  addNewStudent(openCapture = true ){
    if(!openCapture){
      this.setState({studentSelected: false})  
    }
    this.setState({studentCapture: openCapture})
  }

  removeStudent(studentId){
    Axios.post("http://localhost:3002/student/delete", { id: studentId }).then((response) => {
      this.getStudents()
    });
  }

  editStudent(student){
    this.setState({studentSelected: student})
    this.addNewStudent()
  }

  getTopTenAverage(){
    Axios.get("http://localhost:3002/student/tenBestAverages", { crossdomain: true }).then((response) => {
      this.setReportCard(response.data);
    })
  }

  setReportCard(bestStudents){
    const { students } = this.state;

    bestStudents.forEach((bestStudent, index) => {
      let student = students.find((student) => student.id === bestStudent.id);
      bestStudents[index].ReportCards = student.ReportCards;
    });
    
    this.setState({students: bestStudents })
  }
  handleCancel(){
    this.setState({openConfirm: false})
  }

  handleConfirm(){
    const { studentDelete } = this.state;

    this.removeStudent(studentDelete);
    this.setState({openConfirm: false})
    this.setState({studentDelete: false});
  }

  handleOpenConfirmDialog(studentId){
    this.setState({openConfirm: true})
    this.setState({studentDelete: studentId});
  }

  render(){
    const { classes, handleShowErrorAlert } = this.props;
    const {students, studentCapture, courses, studentSelected, openConfirm } = this.state;

    return(
      <section>
        { !studentCapture && (
          <section>
            <section className={classes.headerActionsContainer}>
              <Button color="primary" onClick={this.addNewStudent}>Agregar alumno</Button>
              <Button color="secondary" onClick={this.getTopTenAverage}>Filtrar Top 10</Button>
            </section>
            <Paper>
              <TableContainer className={classes.tableStudentsContainer}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow>
                      {columns.map((column) => (
                        <TableCell
                          key={column.id}
                          align={column.align}
                          style={{ minWidth: column.minWidth }}
                        >
                          {column.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {students.map((student) => {
                      return (
                        <TableRow hover role="checkbox" tabIndex={-1} key={student.id}>
                          {columns.map((column) => {
                            const value = student[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                { column.id === 'actions' ? (
                                  <section>
                                    <IconButton aria-label="edit" onClick={(e)=>{this.editStudent(student)}}>
                                      <CreateIcon fontSize="small" />
                                    </IconButton>
                                    <IconButton aria-label="delete" onClick={(e)=>{this.handleOpenConfirmDialog(student.id)}}>
                                      <DeleteIcon fontSize="small" />
                                    </IconButton>
                                  </section>
                                ) : (value)}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </Paper>
          </section>
        )}
        <div>
          { studentCapture && 
            <StudentForm
              student={studentSelected}
              courses={courses} 
              addNewStudent={this.addNewStudent}
              getStudents={this.getStudents}
              handleShowAlert={this.props.handleShowAlert}
              handleShowErrorAlert={handleShowErrorAlert}
            />}
        </div>
        <Dialog maxWidth="md" open={openConfirm}>
          <DialogTitle id="confirmation-dialog-title">Confirmacion</DialogTitle>
          <DialogContentText>
            ¿Deseas eliminar el registro?
          </DialogContentText>
          <DialogActions>
            <Button autoFocus onClick={this.handleCancel} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleConfirm} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </section>
    )
  }
}

export default withStyles(styles)(Students);
import HeaderApp from './components/HeaderApp'
import FooterApp from './components/FooterApp'

function App() {
  return (
    <div className="App">
      <HeaderApp />
      <FooterApp />
    </div>
  );
}

export default App;

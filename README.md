# Evaluación para responsable de pruebas automatizadas

```
$ git clone git@gitlab.com:clariimedina/examen_qa_concredito.git
$ cd server
$ npm install
$ npm run devStart

En otra ventana de la terminal

$ cd examen_qa_concredito
$ cd client
$ yarn install
$ npm install

$ yarn start

En otra ventana de la terminal

$ cd examen_qa_concredito
$ cd server
$ sequelize db:migrate

ejecutar scrips de inserts
queryKareoke.sql
students.sql
```

Si llegase a haber algun error al correr el comando de yarn install, ejecutar lo siguiente

```
yarn --ignore-engines
```


insert into genders(name) values('Rock');
insert into genders(name) values('Pop');
insert into genders(name) values('Reggaeton');
insert into genders(name) values('Rancheras');
insert into genders(name) values('Regional');
insert into genders(name) values('Salsa');
insert into genders(name) values('Kpop');
insert into genders(name) values('Indie');


insert into artists(name) values('Dua Lipa');
insert into artists(name) values('Maluma');
insert into artists(name) values('Ha-Ash');
insert into artists(name) values('Danna Paola');
insert into artists(name) values('ACDC');
insert into artists(name) values('El Recodo');
insert into artists(name) values('Los tucanes');
insert into artists(name) values('BlackPink');
insert into artists(name) values('Christian Nodal');


insert into songs(name, url, artistId, genderId) values('Levitating', 'https://www.youtube.com/embed/1j_XvebOg4c', 1, 2);
insert into songs(name, url, artistId, genderId) values('Hawai', 'https://www.youtube.com/embed/5kHq6hqgROc', 2, 3 );
insert into songs(name, url, artistId, genderId) values('Se que te vas', 'https://www.youtube.com/embed/eNBTMVfQmvQ', 3, 2);
insert into songs(name, url, artistId, genderId) values('Sodio', 'https://www.youtube.com/embed/_jrml7fmQdA', 4, 2);
insert into songs(name, url, artistId, genderId) values('Mis tres animales', 'https://www.youtube.com/embed/yAXobc879w0', 7, 4);
insert into songs(name, url, artistId, genderId) values('Aqui abajo', 'https://www.youtube.com/embed/3eoDW8RG9gc', 9, 5);
insert into songs(name, url, artistId, genderId) values('Acabame de matar', 'https://www.youtube.com/embed/KPfZVU_oC5U', 6, 4);
insert into songs(name, url, artistId, genderId) values('Highway to hell', 'https://www.youtube.com/embed/B78S3QcVDPg', 5, 1);
insert into songs(name, url, artistId, genderId) values('Ice cream', 'https://www.youtube.com/embed/HDYmBsn16ek', 8, 8);
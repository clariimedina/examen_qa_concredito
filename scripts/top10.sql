SELECT s.id as id, name, lastName, secondLastName, avg(rc.grade) as average, birthDate 
FROM students s JOIN report_card rc 
ON rc.studentId = s.id 
group by rc.studentId 
order by average desc limit 10; 
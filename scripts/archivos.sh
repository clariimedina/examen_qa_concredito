#!/bin/bash

fecha=`date +%Y-%m-%d` #obtener la fecha actual
destino=../../../../ClarissaMedina/home/ubuntu/backups/michangarrito_$fecha/ #directorio destino para la fecha actual
origen=../../../../ClarissaMedina/var/www/michangarrito/ #directorio origen que contiene los archivos
mkdir $destino #crear la directorio destino
cp -r $origen $destino #crear el backup

origen=../../../../ClarissaMedina/home/ubuntu/michangarrito/dist/ #directorio origen
destino=../../../../ClarissaMedina/var/www/michangarrito/ #directorio destino
rm -rf "${destino}/dist" #borrar la directorio dist
mv $origen $destino #actualizar la directorio
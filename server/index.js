const express = require('express')
const studentRouter = require('./routes/student')
const genderRouter = require('./routes/gender')
const artistRouter = require('./routes/artist')
const songRouter = require('./routes/song')
const courseRouter = require('./routes/course')
const app = express()

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use('/student', studentRouter)
app.use('/course', courseRouter)

app.use('/gender', genderRouter)
app.use('/artist', artistRouter)
app.use('/song', songRouter)

app.listen(3002,() => {
  console.log('running on port 3002')
})

const { sequelize, Course  } = require('../models')

module.exports = {
    get: async(req, res) => {
      try{
        const listCourses = await Course.findAll()

        return res.send(listCourses)
      } catch(err) {
        console.log(err)
        return res.status(500).json({error: 'something went wrong '})  
      }
    }
}
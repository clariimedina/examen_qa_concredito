const { sequelize, Student, ReportCard } = require('../models')

module.exports = {
    set: async(req,res) =>{
      const { name, lastName, secondLastName, birthDate, grades } = req.body
      try {
        const user = await Student.create({ 
          name: name,
          lastName: lastName,
          secondLastName:secondLastName,
          birthDate: birthDate
        })

        grades.forEach(grade => {
          let newReportCard =  ReportCard.create({
            grade: grade.grade,
            studentId: user.id,
            courseId: grade.courseId,
          })
        });

        return res.json(req.body)
      } catch(err){
        console.log(err)
        return res.status(500).json(err)
      }
    },
    get: async(req, res) => {
      try{
        const students = await Student.findAll({include: [{model: ReportCard}]}) 

        return res.json(students)
      } catch(err) {
        console.log(err)
        return res.status(500).json({error: 'something went wrong '})  
      }
    },
    update: async(req,res) =>{
      const { name, lastName, secondLastName, birthDate, grades, id } = req.body
      try {
        const user = await Student.update({ 
          name: name,
          lastName: lastName,
          secondLastName:secondLastName,
          birthDate: birthDate
        },{
          where: {id: id}
        })

        grades.forEach(grade => {
          let newReportCard =  ReportCard.update({
            grade: grade.grade
          },{
            where: {id: grade.id}
          })
        });

        return res.json(req.body)
      } catch(err){
        console.log(err)
        return res.status(500).json(err)
      }
    },
    delete: async(req, res)=>{
      const { id } = req.body;      
      try{
        const students = await Student.destroy({where: {id: id}}) 

        return res.json(students)
      } catch(err) {
        console.log(err)
        return res.status(500).json({error: 'something went wrong '})  
      }
    },
    getTenBestAverage: async(req, res )=> {
      try{
        const bestAverages = await sequelize.query(
          " SELECT s.id as id, name, lastName, secondLastName, round(avg(rc.grade), 2) as average, birthDate FROM students s JOIN report_card rc ON rc.studentId = s.id group by rc.studentId order by average desc limit 10; ", {
          model: Student
        });
        return res.json(bestAverages);
      } catch(err){
        console.log(err)
        return res.status(500).json({error: 'something went wrong '})  
      }
    }
}
const { sequelize, Song, Gender, Artist } = require('../models')

module.exports = {
  set: async(req,res) =>{
    const { name, url, genderId, artistId } = req.body
    try {
      const song = await Song.create({ 
        name: name,
        url: url,
        genderId:genderId,
        artistId: artistId
      })

      return res.json(req.body)
    } catch(err){
      console.log(err)
      return res.status(500).json(err)
    }
  },
  get: async(req, res) => {
    try{
      const songs = await Song.findAll({include: [Gender, Artist]})

      return res.json(songs)
    } catch(err) {
      console.log(err)
      return res.status(500).json({error: 'something went wrong '})  
    }
  },
  delete: async(req, res) => {
    const { id } = req.body;      
      try{
        const songs = await Song.destroy({where: {id: id}}) 

        return res.json(songs);
      } catch(err) {
        console.log(err)
        return res.status(500).json({error: 'something went wrong '})  
      }
  },
  update: async(req,res) =>{
    const { name, url, genderId, artistId, id } = req.body
    
    try {
      const song = await Song.update({ 
        name: name,
        url: url,
        genderId:genderId,
        artistId: artistId
      },{
        where: {id: id}
      })
      return res.json(req.body)
    } catch(err){
      console.log(err)
      return res.status(500).json(err)
    }
  },
}
const { sequelize, Gender  } = require('../models')

module.exports = {
  set: async(req,res) =>{
    const { name } = req.body
    try {
      const gender = await Gender.create({ 
        name: name
      })

      return res.json(req.body)
    } catch(err){
      console.log(err)
      return res.status(500).json(err)
    }
  },
  get: async(req, res) => {
    try{
      const genders = await Gender.findAll()

      return res.send(genders)
    } catch(err) {
      console.log(err)
      return res.status(500).json({error: 'something went wrong '})  
    }
  }
}
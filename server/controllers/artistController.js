const { sequelize, Artist  } = require('../models')

module.exports = {
  set: async(req,res) =>{
    const { name } = req.body
    console.log('dasdlas');
    try {
      const artist = await Artist.create({ 
        name: name
      })

      console.log(artist);
      
      return res.json(req.body)
    } catch(err){
      console.log(err)
      return res.status(500).json(err)
    }
  },
  get: async(req, res) => {
    try{
      const artists = await Artist.findAll()

      return res.json(artists)
    } catch(err) {
      console.log(err)
      return res.status(500).json({error: 'something went wrong '})  
    }
  }
}
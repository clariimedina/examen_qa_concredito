'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Student extends Model {
    static associate({ReportCard}) {
      this.hasMany(ReportCard, {foreignKey: 'studentId'})
    }
  };
  Student.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    secondLastName: DataTypes.STRING,
    birthDate: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Student',
  });
  return Student;
};
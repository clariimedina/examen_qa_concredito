'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Course extends Model {
    static associate({ ReportCard }) {
      this.hasMany(ReportCard, {foreignKey: 'courseId'})
    }
  };
  Course.init({
    Name: DataTypes.STRING
  },{
    sequelize,
    timestamps: false,
    tableName: 'courses',
    modelName: 'Course',
  });
  return Course;
};
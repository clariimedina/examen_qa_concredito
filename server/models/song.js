'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Song extends Model {
    static associate({Artist, Gender}) {
      this.belongsTo(Artist, {foreignKey: 'artistId'})
      this.belongsTo(Gender, {foreignKey: 'genderId'})
    }
  };
  Song.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    artistId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    genderId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Song',
  });
  return Song;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ReportCard extends Model {
    static associate({ Student, Course }) {
      this.belongsTo(Student, {foreignKey: 'studentId'})
      this.belongsTo(Course, {foreignKey: 'courseId'})
    }
  };
  ReportCard.init({
    grade: DataTypes.DOUBLE,
    studentId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    courseId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {
    sequelize,
    tableName: 'report_card',
    modelName: 'ReportCard',
  });
  return ReportCard;
};
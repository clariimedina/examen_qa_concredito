const express = require('express')
const controller = require('../controllers/courseController')

const router = express.Router()

router.get('/courses', controller.get)


module.exports = router;
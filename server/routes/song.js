const express = require('express')
const controller = require('../controllers/songController')
const bodyParser = require('body-parser');

const router = express.Router()
var jsonParser = bodyParser.json()

router.post('/', jsonParser, controller.set)
router.post('/edit', jsonParser, controller.update)
router.post('/delete', jsonParser, controller.delete)
router.get('/songs', controller.get)


module.exports = router;
const express = require('express')
const controller = require('../controllers/genderController')
const bodyParser = require('body-parser');

const router = express.Router()
var jsonParser = bodyParser.json()

router.get('/genders', controller.get)
router.post('/',jsonParser, controller.set);


module.exports = router;
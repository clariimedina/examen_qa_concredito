const express = require('express')
const controller = require('../controllers/studentController')
const router = express.Router()
const bodyParser = require('body-parser');

var jsonParser = bodyParser.json()

router.post('/', jsonParser, controller.set)
router.get('/students', controller.get)
router.post('/delete', jsonParser, controller.delete)
router.post('/edit', jsonParser, controller.update)
router.get('/tenBestAverages', controller.getTenBestAverage)



module.exports = router;
'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('report_card', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      grade: {
        type: Sequelize.DOUBLE
      },
      studentId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      courseId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      }
    });
    /*
    await queryInterface.renameColumn('report_card', 'studentId', 'student_id')
    await queryInterface.renameColumn('report_card', 'createdAt', 'created_at')
    await queryInterface.renameColumn('report_card', 'updatedAt', 'updated_at')
    await queryInterface.renameColumn('report_card', 'courseId', 'course_id')*/
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('report_card');
  }
};
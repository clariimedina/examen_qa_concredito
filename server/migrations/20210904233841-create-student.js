'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('students', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      secondLastName: {
        type: Sequelize.STRING
      },
      birthDate: {
        type: Sequelize.DATE
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      }
    });
    /*
    await queryInterface.renameColumn('students', 'lastName', 'last_name')
    await queryInterface.renameColumn('students', 'secondLastName', 'second_last_name')
    await queryInterface.renameColumn('students', 'updatedAt', 'updated_at')
    await queryInterface.renameColumn('students', 'createdAt', 'created_at')
    await queryInterface.renameColumn('students', 'birthDate', 'birth_date') */
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('students');
  }
};